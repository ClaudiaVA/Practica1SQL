/* 1. Crear la BD */
CREATE DATABASE Practica1

/* 2. Eliminar la BD */
DROP DATABASE Practica1

/* 3. Crear las tablas: Clientes, Productos, y Ventas */
CREATE TABLE Clientes(Nombres varchar(50)NULL,Apellidos varchar(50)NULL,Edad int NULL)
CREATE TABLE Productos(CodProducto int NOT NULL,Nombre varchar(50)NOT NULL,Precio money NULL)
CREATE TABLE Ventas(IdProducto int NOT NULL,IdCliente int NOT NULL,Fecha date NOT NULL)

/* 4. Eliminar las tablas creadas en el punto 3) */

DROP TABLE Clientes
DROP TABLE Productos
DROP TABLE Ventas
 
/* 5. Modificar el nombre de la columna �Apellido� por �Apellidos� en la tabla  Clientes */

EXEC sp_rename 'Clientes.Apellido', 'Apellidos'

/* 6. Vaciar (eliminar todos los registros) de la tabla Ventas */
TRUNCATE TABLE VENTAS

/* 7. Insertar 5 registros en la tablas Clientes, Productos, y Ventas */

INSERT INTO Clientes (Nombres,Apellidos,Edad) VALUES('Nombre1','Apellido1',20)
INSERT INTO Clientes (Nombres,Apellidos,Edad) VALUES('Nombre2','Apellido2',24)
INSERT INTO Productos (CodProducto, Nombre, Precio) VALUES(1, 'Producto1', 10)
INSERT INTO Productos (CodProducto, Nombre, Precio) VALUES(2, 'Producto2', 30)
INSERT INTO Ventas (IdProducto,IdCliente,Fecha) VALUES (101,1,'2018-08-19')

/* 8. Listar todas las ventas */
SELECT * FROM Ventas

/* 9.Listar los primeros 10 clientes */
SELECT TOP 10 * FROM Clientes

/* 10. Listar los clientes agrupados por Edad */
SELECT * FROM Clientes GROUP BY Edad

/* 11. Listar los clientes cuyas edades sean mayores a 20 a�os */
SELECT * FROM Clientes WHERE Edad > 20;

/* 12. Eliminar los clientes cuyas edades sean mayores o igual a 20 a�os  */
DELETE FROM Clientes WHERE Edad>=20; 

